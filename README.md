# go-iecp5

go-iecp5 library for IEC 60870-5 based protocols in pure go.
The current implementation contains code for IEC 60870-5-104 (protocool over TCP/IP) specifications.


[![Go.Dev reference](https://img.shields.io/badge/go.dev-reference-blue?logo=go&logoColor=white)](https://pkg.go.dev/gitlab.com/circutor-library/go-iecp5?tab=doc)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/circutor-library/go-iecp5)](https://goreportcard.com/report/gitlab.com/circutor-library/go-iecp5)
[![License](https://img.shields.io/gitlab/license/circutor-library/go-iecp5)](https://gitlab.com/circutor-library/go-iecp5/raw/master/LICENSE)


asdu package: [![GoDoc](https://godoc.org/github.com/thinkgos/go-iecp5/asdu?status.svg)](https://godoc.org/gitlab.com/circutor-library/go-iecp5/asdu)  
cs104 package: [![GoDoc](https://godoc.org/github.com/thinkgos/go-iecp5/cs104?status.svg)](https://godoc.org/gitlab.com/circutor-library/go-iecp5/cs104)  

## Feature:

- client/server for CS 104 TCP/IP communication
- support for much application layer(except file object) message types,

# Reference:

lib60870 c library [lib60870](https://github.com/mz-automation/lib60870)  
lib60870 c library doc [lib60870 doc](https://support.mz-automation.de/doc/lib60870/latest/group__CS104__MASTER.html)