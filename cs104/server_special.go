// Copyright 2020 thinkgos (thinkgo@aliyun.com).  All rights reserved.
// Use of this source code is governed by a version 3 of the GNU General
// Public License, license that can be found in the LICENSE file.

package cs104

import (
	"context"
	"crypto/rand"
	"errors"
	"log/slog"
	"math/big"
	"sync/atomic"
	"time"

	"gitlab.com/circutor-library/go-iecp5/asdu"
	"gitlab.com/circutor-library/go-iecp5/clog"
)

// ServerSpecial server special interface
type ServerSpecial interface {
	asdu.Connect

	IsConnected() bool
	IsClosed() bool
	Start() error
	Close() error

	SetOnConnectHandler(f func(c asdu.Connect))
	SetConnectionLostHandler(f func(c asdu.Connect))
}

type serverSpec struct {
	SrvSession
	option      ClientOption
	closeCancel context.CancelFunc
}

// NewServerSpecial new special server
func NewServerSpecial(handler ServerHandlerInterface, o *ClientOption) ServerSpecial {
	clog.NewLogger()

	return &serverSpec{
		SrvSession: SrvSession{
			config:  &o.config,
			params:  &o.params,
			handler: handler,

			rcvASDU:  make(chan []byte, 1024),
			sendASDU: make(chan []byte, 1024),
			rcvRaw:   make(chan []byte, 1024),
			sendRaw:  make(chan []byte, 1024), // may not block!

		},
		option: *o,
	}
}

// SetOnConnectHandler set on connect handler
func (sf *serverSpec) SetOnConnectHandler(f func(conn asdu.Connect)) {
	sf.onConnection = f
}

// SetConnectionLostHandler set connection lost handler
func (sf *serverSpec) SetConnectionLostHandler(f func(c asdu.Connect)) {
	sf.connectionLost = f
}

// Start start the server,and return quickly,if it nil,the server will disconnected background,other failed
func (sf *serverSpec) Start() error {
	if sf.option.server == nil {
		return errors.New("empty remote server")
	}

	go sf.running()

	return nil
}

// Increase reconnect interval
func (sf *serverSpec) running() {
	var ctx context.Context

	sf.rwMux.Lock()
	if !atomic.CompareAndSwapUint32(&sf.status, initial, disconnected) {
		sf.rwMux.Unlock()

		return
	}

	ctx, sf.closeCancel = context.WithCancel(context.Background())
	sf.rwMux.Unlock()

	defer sf.setConnectStatus(initial)

	for {
		select {
		case <-ctx.Done():
			return
		default:
		}

		slog.Debug("connecting server", "server", sf.option.server)

		conn, err := openConnection(sf.option.server, sf.option.TLSConfig, sf.config.ConnectTimeout0)
		if err != nil {
			slog.Error("connect failed", "error", err)
			if !sf.option.autoReconnect {
				return
			}

			time.Sleep(sf.option.reconnectInterval)

			continue
		}
		slog.Debug("connect success")
		sf.conn = conn
		sf.run(ctx)
		slog.Debug("disconnected server", "server", sf.option.server)

		select {
		case <-ctx.Done():
			return
		default:
			// Random 500ms-1s retries to avoid fast retries causing many invalid connections to the server
			rnd, err := rand.Int(rand.Reader, big.NewInt(500))
			if err != nil {
				rnd = big.NewInt(0)
			}

			time.Sleep(time.Millisecond * time.Duration(500+rnd.Int64()))
		}
	}
}

func (sf *serverSpec) IsClosed() bool {
	return sf.connectStatus() == initial
}

func (sf *serverSpec) Close() error {
	sf.rwMux.Lock()

	if sf.closeCancel != nil {
		sf.closeCancel()
	}

	sf.rwMux.Unlock()

	return nil
}
