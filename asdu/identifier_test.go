package asdu_test

import (
	"reflect"
	"testing"

	"gitlab.com/circutor-library/go-iecp5/asdu"
)

func TestGetInfoObjSize(t *testing.T) {
	type args struct {
		id asdu.TypeID
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{"defined", args{asdu.F_DR_TA_1}, 13, false},
		{"no defined", args{asdu.F_SG_NA_1}, 0, true},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := asdu.GetInfoObjSize(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetInfoObjSize() error = %v, wantErr %v", err, tt.wantErr)

				return
			}

			if got != tt.want {
				t.Errorf("GetInfoObjSize() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTypeID_String(t *testing.T) {
	tests := []struct {
		name string
		this asdu.TypeID
		want string
	}{
		{"M_SP_NA_1", asdu.M_SP_NA_1, "TID<M_SP_NA_1>"},
		{"M_SP_TB_1", asdu.M_SP_TB_1, "TID<M_SP_TB_1>"},
		{"C_SC_NA_1", asdu.C_SC_NA_1, "TID<C_SC_NA_1>"},
		{"C_SC_TA_1", asdu.C_SC_TA_1, "TID<C_SC_TA_1>"},
		{"M_EI_NA_1", asdu.M_EI_NA_1, "TID<M_EI_NA_1>"},
		{"S_CH_NA_1", asdu.S_CH_NA_1, "TID<S_CH_NA_1>"},
		{"S_US_NA_1", asdu.S_US_NA_1, "TID<S_US_NA_1>"},
		{"C_IC_NA_1", asdu.C_IC_NA_1, "TID<C_IC_NA_1>"},
		{"P_ME_NA_1", asdu.P_ME_NA_1, "TID<P_ME_NA_1>"},
		{"F_FR_NA_1", asdu.F_FR_NA_1, "TID<F_FR_NA_1>"},
		{"no defined", 0, "TID<0>"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.this.String(); got != tt.want {
				t.Errorf("TypeID.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParseVariableStruct(t *testing.T) {
	type args struct {
		b byte
	}
	tests := []struct {
		name string
		args args
		want asdu.VariableStruct
	}{
		{"no sequence", args{0x0a}, asdu.VariableStruct{Number: 0x0a}},
		{"with sequence", args{0x8a}, asdu.VariableStruct{Number: 0x0a, IsSequence: true}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := asdu.ParseVariableStruct(tt.args.b); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseVariableStruct() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestVariableStruct_Value(t *testing.T) {
	tests := []struct {
		name string
		this asdu.VariableStruct
		want byte
	}{
		{"no sequence", asdu.VariableStruct{Number: 0x0a}, 0x0a},
		{"with sequence", asdu.VariableStruct{Number: 0x0a, IsSequence: true}, 0x8a},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.this.Value(); got != tt.want {
				t.Errorf("VariableStruct.Value() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestVariableStruct_String(t *testing.T) {
	tests := []struct {
		name string
		this asdu.VariableStruct
		want string
	}{
		{"no sequence", asdu.VariableStruct{Number: 100}, "VSQ<100>"},
		{"with sequence", asdu.VariableStruct{Number: 100, IsSequence: true}, "VSQ<sq,100>"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.this.String(); got != tt.want {
				t.Errorf("VariableStruct.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParseCauseOfTransmission(t *testing.T) {
	type args struct {
		b byte
	}
	tests := []struct {
		name string
		args args
		want asdu.CauseOfTransmission
	}{
		{"no test and neg", args{0x01}, asdu.CauseOfTransmission{Cause: asdu.Periodic}},
		{"with test", args{0x81}, asdu.CauseOfTransmission{Cause: asdu.Periodic, IsTest: true}},
		{"with neg", args{0x41}, asdu.CauseOfTransmission{Cause: asdu.Periodic, IsNegative: true}},
		{"with test and neg", args{0xc1}, asdu.CauseOfTransmission{Cause: asdu.Periodic, IsTest: true, IsNegative: true}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := asdu.ParseCauseOfTransmission(tt.args.b); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseCauseOfTransmission() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCauseOfTransmission_Value(t *testing.T) {
	tests := []struct {
		name string
		this asdu.CauseOfTransmission
		want byte
	}{
		{"no test and neg", asdu.CauseOfTransmission{Cause: asdu.Periodic}, 0x01},
		{"with test", asdu.CauseOfTransmission{Cause: asdu.Periodic, IsTest: true}, 0x81},
		{"with neg", asdu.CauseOfTransmission{Cause: asdu.Periodic, IsNegative: true}, 0x41},
		{"with test and neg", asdu.CauseOfTransmission{Cause: asdu.Periodic, IsTest: true, IsNegative: true}, 0xc1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.this.Value(); got != tt.want {
				t.Errorf("CauseOfTransmission.Value() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCauseOfTransmission_String(t *testing.T) {
	tests := []struct {
		name string
		this asdu.CauseOfTransmission
		want string
	}{
		{"no test and neg", asdu.CauseOfTransmission{Cause: asdu.Periodic}, "COT<Periodic>"},
		{"with test", asdu.CauseOfTransmission{Cause: asdu.Periodic, IsTest: true}, "COT<Periodic,test>"},
		{"with neg", asdu.CauseOfTransmission{Cause: asdu.Periodic, IsNegative: true}, "COT<Periodic,neg>"},
		{"with test and neg", asdu.CauseOfTransmission{Cause: asdu.Periodic, IsTest: true, IsNegative: true}, "COT<Periodic,neg,test>"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.this.String(); got != tt.want {
				t.Errorf("CauseOfTransmission.String() = %v, want %v", got, tt.want)
			}
		})
	}
}
